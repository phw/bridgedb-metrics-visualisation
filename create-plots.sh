#!/usr/bin/env bash

# Directory where our plots are written to.
PLOT_OUTPUT=plots
# Directory where our CSV input files are coming from.
PLOT_INPUT=raw-plot-files
# Directory where our original metrics file are.
METRICS_FILES=metrics-files

mkdir -p "$PLOT_INPUT" "$PLOT_OUTPUT"

function extract_metrics {
    output=$1
    type=$2
    dist=$3
    origin=$4
    success=$5

    echo "timestamp,lower,upper" > "$output"
    for file in $(ls $METRICS_FILES/*)
    do
        bridgedb-metrics-parser \
            -b "$type" \
            -d "$dist" \
            -o "$origin" \
            -s "$success" \
            -f "$file" >> "$output"
    done
}

#-------------------------------------------------------------------------------

CSV_FILE1="$PLOT_INPUT/Obfs4"
CSV_FILE2="$PLOT_INPUT/Obfs3"
CSV_FILE3="$PLOT_INPUT/Vanilla"
CSV_FILE4="$PLOT_INPUT/ScrambleSuit"
CSV_FILE5="$PLOT_INPUT/FTE"

extract_metrics "$CSV_FILE1" "obfs4" "" "^[^z][^z]$" ""
extract_metrics "$CSV_FILE2" "obfs3" "" "^[^z][^z]$" ""
extract_metrics "$CSV_FILE3" "vanilla" "" "^[^z][^z]$" ""
extract_metrics "$CSV_FILE4" "scramblesuit" "" "^[^z][^z]$" ""
extract_metrics "$CSV_FILE5" "fte" "" "^[^z][^z]$" ""

./plot-bridgedb-metrics.R -t "Requests per bridge type without Tor over HTTPS" \
                          -o "${PLOT_OUTPUT}/bridge-types-nontor.png" \
                          "$CSV_FILE1" "$CSV_FILE2" "$CSV_FILE3" "$CSV_FILE4" \
                          "$CSV_FILE5"

#-------------------------------------------------------------------------------

CSV_FILE1="$PLOT_INPUT/Obfs4"
CSV_FILE2="$PLOT_INPUT/Obfs3"
CSV_FILE3="$PLOT_INPUT/Vanilla"
CSV_FILE4="$PLOT_INPUT/ScrambleSuit"
CSV_FILE5="$PLOT_INPUT/FTE"

extract_metrics "$CSV_FILE1" "obfs4" "" "" ""
extract_metrics "$CSV_FILE2" "obfs3" "" "" ""
extract_metrics "$CSV_FILE3" "vanilla" "" "" ""
extract_metrics "$CSV_FILE4" "scramblesuit" "" "" ""
extract_metrics "$CSV_FILE5" "fte" "" "" ""

./plot-bridgedb-metrics.R -t "Requests per bridge type" \
                          -o "${PLOT_OUTPUT}/bridge-types.png" \
                          "$CSV_FILE1" "$CSV_FILE2" "$CSV_FILE3" "$CSV_FILE4" \
                          "$CSV_FILE5"

#-------------------------------------------------------------------------------

CSV_FILE1="$PLOT_INPUT/HTTPS"
CSV_FILE2="$PLOT_INPUT/Email"
CSV_FILE3="$PLOT_INPUT/Moat"

extract_metrics "$CSV_FILE1" "" "https" "" ""
extract_metrics "$CSV_FILE2" "" "email" "" ""
extract_metrics "$CSV_FILE3" "" "moat" "" ""

./plot-bridgedb-metrics.R -t "Distribution mechanisms" \
                          -o "${PLOT_OUTPUT}/distribution-mechanisms.png" \
                          "$CSV_FILE1" "$CSV_FILE2" "$CSV_FILE3"

#-------------------------------------------------------------------------------

CSV_FILE1="$PLOT_INPUT/HTTPS"
CSV_FILE2="$PLOT_INPUT/Email"
CSV_FILE3="$PLOT_INPUT/Moat"

extract_metrics "$CSV_FILE1" "" "https" "^[^z][^z]$" ""
extract_metrics "$CSV_FILE2" "" "email" "" ""
extract_metrics "$CSV_FILE3" "" "moat" "" ""

./plot-bridgedb-metrics.R -t "Distribution mechanisms without Tor over HTTPS" \
                          -o "${PLOT_OUTPUT}/distribution-mechanisms-notor.png" \
                          "$CSV_FILE1" "$CSV_FILE2" "$CSV_FILE3"

#-------------------------------------------------------------------------------

CSV_FILE1="$PLOT_INPUT/Success"
CSV_FILE2="$PLOT_INPUT/Fail"

extract_metrics "$CSV_FILE1" "" "email" "" "success"
extract_metrics "$CSV_FILE2" "" "email" "" "fail"

./plot-bridgedb-metrics.R -t "(Un)successful email requests" \
                          -o "${PLOT_OUTPUT}/(un)successful-email.png" \
                          "$CSV_FILE1" "$CSV_FILE2"

#-------------------------------------------------------------------------------

CSV_FILE1="$PLOT_INPUT/Success"
CSV_FILE2="$PLOT_INPUT/Fail"

extract_metrics "$CSV_FILE1" "" "moat" "" "success"
extract_metrics "$CSV_FILE2" "" "moat" "" "fail"

./plot-bridgedb-metrics.R -t "(Un)successful moat requests" \
                          -o "${PLOT_OUTPUT}/(un)successful-moat.png" \
                          "$CSV_FILE1" "$CSV_FILE2"

#-------------------------------------------------------------------------------

CSV_FILE1="$PLOT_INPUT/Success"
CSV_FILE2="$PLOT_INPUT/Fail"

extract_metrics "$CSV_FILE1" "" "https" "^[^z][^z]$" "success"
extract_metrics "$CSV_FILE2" "" "https" "^[^z][^z]$" "fail"

./plot-bridgedb-metrics.R -t "(Un)successful HTTPS requests (not over Tor)" \
                          -o "${PLOT_OUTPUT}/(un)successful-nontor-https.png" \
                          "$CSV_FILE1" "$CSV_FILE2"

#-------------------------------------------------------------------------------

CSV_FILE1="$PLOT_INPUT/Success"
CSV_FILE2="$PLOT_INPUT/Fail"

extract_metrics "$CSV_FILE1" "" "https" "zz" "success"
extract_metrics "$CSV_FILE2" "" "https" "zz" "fail"

./plot-bridgedb-metrics.R -t "(Un)successful HTTPS requests (over Tor)" \
                          -o "${PLOT_OUTPUT}/(un)successful-tor-https.png" \
                          "$CSV_FILE1" "$CSV_FILE2"

#-------------------------------------------------------------------------------

output="${PLOT_INPUT}/country-proportions.csv"
echo "cc,timestamp,lower,upper" > "$output"
IFS=$'\n'
for cc in ae cn eg et ir kz sy tr ve
do
    echo "Processing country code $cc"
    for file in $(ls metrics-files/*)
    do
        echo -n "${cc}," >> "$output"
        bridgedb-metrics-parser \
            -d "https" \
            -o "$cc" \
            -f "$file" >> "$output"
    done
done

output="${PLOT_INPUT}/per-country-requests.csv"
echo "cc,timestamp,lower,upper" > "$output"
IFS=$'\n'
for cc in $(cat country-codes.txt)
do
    echo "Processing country code $cc"
    for file in $(ls metrics-files/*)
    do
        echo -n "${cc}," >> "$output"
        bridgedb-metrics-parser \
            -d "https" \
            -o "$cc" \
            -f "$file" >> "$output"
    done
done
