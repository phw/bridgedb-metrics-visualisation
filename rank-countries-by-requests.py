#!/usr/bin/env python3
#
# Take as input a file that has the following format:
#
#     cc,timestamp,lower,upper
#     ad,2019-09-05 08:51:16 +0000 UTC,0,0
#     ad,2019-09-06 08:51:16 +0000 UTC,0,0
#     ad,2019-09-07 08:51:16 +0000 UTC,0,0
#     ...
#     zw,2020-07-28 21:28:05 +0000 UTC,0,0
#     zw,2020-07-29 21:28:05 +0000 UTC,0,0
#     zw,2020-07-30 21:28:05 +0000 UTC,0,0
#
# ...and determine the per-country median values.

import csv
import sys
import statistics

COUNTRY_CODES = "country-codes.txt"

cc2nums = {}


def populate_dict():
    with open(COUNTRY_CODES) as fd:
        for cc in fd.readlines():
            cc2nums[cc.strip()] = []


def process_file(filename):
    with open(filename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            cc2nums[row["cc"]].append(int(row["lower"]))


def rank_countries():
    medians = {k: statistics.median(v) for k, v in cc2nums.items()}

    print("| Country code | Median |")
    print("| :--- | ---: |")
    for cc, num in sorted(medians.items(),
                          key=lambda item: item[1],
                          reverse=True):
        if num > 0:
            print("| %s | %d |" % (cc, num))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: %s FILENAME" % sys.argv[0])
        sys.exit(1)

    populate_dict()
    process_file(sys.argv[1])
    rank_countries()
