METRICS_DIR = metrics-files

data:
	wget -nd -np -q -P $(METRICS_DIR) -r https://collector.torproject.org/recent/bridgedb-metrics/
	rm $(METRICS_DIR)/*index*

build: bridgedb-metrics-parser.go
	GO111MODULE=off go install

plots: create-plots.sh
	./create-plots.sh
